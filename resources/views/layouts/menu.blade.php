<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{ route('users.index') }}"><i class="fa fa-edit"></i><span>Usuarios</span></a>
</li><li class="{{ Request::is('personas*') ? 'active' : '' }}">
    <a href="{{ route('personas.index') }}"><i class="fa fa-edit"></i><span>Personas</span></a>
</li>

<li class="{{ Request::is('rols*') ? 'active' : '' }}">
    <a href="{{ route('rols.index') }}"><i class="fa fa-edit"></i><span>Rols</span></a>
</li>

<li class="{{ Request::is('ciudads*') ? 'active' : '' }}">
    <a href="{{ route('ciudads.index') }}"><i class="fa fa-edit"></i><span>Ciudads</span></a>
</li>

<li class="{{ Request::is('vehiculos*') ? 'active' : '' }}">
    <a href="{{ route('vehiculos.index') }}"><i class="fa fa-edit"></i><span>Vehiculos</span></a>
</li>

<li class="{{ Request::is('personaPorVehiculos*') ? 'active' : '' }}">
    <a href="{{ route('personaPorVehiculos.index') }}"><i class="fa fa-edit"></i><span>Persona Por  Vehiculos</span></a>
</li>

<li class="{{ Request::is('marcas*') ? 'active' : '' }}">
    <a href="{{ route('marcas.index') }}"><i class="fa fa-edit"></i><span>Marcas</span></a>
</li>

