<!-- Cedula Field -->
<div class="form-group">
    {!! Form::label('cedula', 'Cedula:') !!}
    <p>{{ $persona->cedula }}</p>
</div>

<!-- Primer Nombre Field -->
<div class="form-group">
    {!! Form::label('primer_nombre', 'Primer Nombre:') !!}
    <p>{{ $persona->primer_nombre }}</p>
</div>

<!-- Segundo Nombre Field -->
<div class="form-group">
    {!! Form::label('segundo_nombre', 'Segundo Nombre:') !!}
    <p>{{ $persona->segundo_nombre }}</p>
</div>

<!-- Apellidos Field -->
<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos:') !!}
    <p>{{ $persona->apellidos }}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{{ $persona->direccion }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{{ $persona->telefono }}</p>
</div>

<!-- Ciudad Id Field -->
<div class="form-group">
    {!! Form::label('ciudad_id', 'Ciudad Id:') !!}
    <p>{{ $persona->ciudad_id }}</p>
</div>

<!-- Remember Token Field -->
<div class="form-group">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    <p>{{ $persona->remember_token }}</p>
</div>

<!-- Rols Id Field -->
<div class="form-group">
    {!! Form::label('rols_id', 'Rols Id:') !!}
    <p>{{ $persona->rols_id }}</p>
</div>

<!-- Contraseña Field -->
<div class="form-group">
    {!! Form::label('contraseña', 'Contraseña:') !!}
    <p>{{ $persona->contraseña }}</p>
</div>

