<!-- Placa Field -->
<div class="form-group">
    {!! Form::label('placa', 'Placa:') !!}
    <p>{{ $vehiculo->placa }}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{{ $vehiculo->color }}</p>
</div>

<!-- Tipo Field -->
<div class="form-group">
    {!! Form::label('tipo', 'Tipo:') !!}
    <p>{{ $vehiculo->tipo }}</p>
</div>

<!-- Marcas Id Field -->
<div class="form-group">
    {!! Form::label('marcas_id', 'Marcas Id:') !!}
    <p>{{ $vehiculo->marcas_id }}</p>
</div>

