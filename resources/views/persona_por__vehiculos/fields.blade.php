<!-- Vehiculos Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vehiculos_id', 'Vehiculos Id:') !!}
    {!! Form::number('vehiculos_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Personas Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('personas_id', 'Personas Id:') !!}
    {!! Form::number('personas_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('personaPorVehiculos.index') }}" class="btn btn-default">Cancel</a>
</div>
