@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Persona Por  Vehiculo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($personaPorVehiculo, ['route' => ['personaPorVehiculos.update', $personaPorVehiculo->id], 'method' => 'patch']) !!}

                        @include('persona_por__vehiculos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection