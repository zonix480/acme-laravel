<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Vehiculo;

class VehiculoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/vehiculos', $vehiculo
        );

        $this->assertApiResponse($vehiculo);
    }

    /**
     * @test
     */
    public function test_read_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/vehiculos/'.$vehiculo->id
        );

        $this->assertApiResponse($vehiculo->toArray());
    }

    /**
     * @test
     */
    public function test_update_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();
        $editedVehiculo = factory(Vehiculo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/vehiculos/'.$vehiculo->id,
            $editedVehiculo
        );

        $this->assertApiResponse($editedVehiculo);
    }

    /**
     * @test
     */
    public function test_delete_vehiculo()
    {
        $vehiculo = factory(Vehiculo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/vehiculos/'.$vehiculo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/vehiculos/'.$vehiculo->id
        );

        $this->response->assertStatus(404);
    }
}
