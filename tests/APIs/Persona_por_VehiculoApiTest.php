<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Persona_por_Vehiculo;

class Persona_por_VehiculoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/persona_por__vehiculos', $personaPorVehiculo
        );

        $this->assertApiResponse($personaPorVehiculo);
    }

    /**
     * @test
     */
    public function test_read_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/persona_por__vehiculos/'.$personaPorVehiculo->id
        );

        $this->assertApiResponse($personaPorVehiculo->toArray());
    }

    /**
     * @test
     */
    public function test_update_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();
        $editedPersona_por_Vehiculo = factory(Persona_por_Vehiculo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/persona_por__vehiculos/'.$personaPorVehiculo->id,
            $editedPersona_por_Vehiculo
        );

        $this->assertApiResponse($editedPersona_por_Vehiculo);
    }

    /**
     * @test
     */
    public function test_delete_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/persona_por__vehiculos/'.$personaPorVehiculo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/persona_por__vehiculos/'.$personaPorVehiculo->id
        );

        $this->response->assertStatus(404);
    }
}
