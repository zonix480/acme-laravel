<?php namespace Tests\Repositories;

use App\Models\Layer;
use App\Repositories\LayerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LayerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LayerRepository
     */
    protected $layerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->layerRepo = \App::make(LayerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_layer()
    {
        $layer = factory(Layer::class)->make()->toArray();

        $createdLayer = $this->layerRepo->create($layer);

        $createdLayer = $createdLayer->toArray();
        $this->assertArrayHasKey('id', $createdLayer);
        $this->assertNotNull($createdLayer['id'], 'Created Layer must have id specified');
        $this->assertNotNull(Layer::find($createdLayer['id']), 'Layer with given id must be in DB');
        $this->assertModelData($layer, $createdLayer);
    }

    /**
     * @test read
     */
    public function test_read_layer()
    {
        $layer = factory(Layer::class)->create();

        $dbLayer = $this->layerRepo->find($layer->id);

        $dbLayer = $dbLayer->toArray();
        $this->assertModelData($layer->toArray(), $dbLayer);
    }

    /**
     * @test update
     */
    public function test_update_layer()
    {
        $layer = factory(Layer::class)->create();
        $fakeLayer = factory(Layer::class)->make()->toArray();

        $updatedLayer = $this->layerRepo->update($fakeLayer, $layer->id);

        $this->assertModelData($fakeLayer, $updatedLayer->toArray());
        $dbLayer = $this->layerRepo->find($layer->id);
        $this->assertModelData($fakeLayer, $dbLayer->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_layer()
    {
        $layer = factory(Layer::class)->create();

        $resp = $this->layerRepo->delete($layer->id);

        $this->assertTrue($resp);
        $this->assertNull(Layer::find($layer->id), 'Layer should not exist in DB');
    }
}
