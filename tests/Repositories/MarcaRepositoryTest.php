<?php namespace Tests\Repositories;

use App\Models\Marca;
use App\Repositories\MarcaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MarcaRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MarcaRepository
     */
    protected $marcaRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->marcaRepo = \App::make(MarcaRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_marca()
    {
        $marca = factory(Marca::class)->make()->toArray();

        $createdMarca = $this->marcaRepo->create($marca);

        $createdMarca = $createdMarca->toArray();
        $this->assertArrayHasKey('id', $createdMarca);
        $this->assertNotNull($createdMarca['id'], 'Created Marca must have id specified');
        $this->assertNotNull(Marca::find($createdMarca['id']), 'Marca with given id must be in DB');
        $this->assertModelData($marca, $createdMarca);
    }

    /**
     * @test read
     */
    public function test_read_marca()
    {
        $marca = factory(Marca::class)->create();

        $dbMarca = $this->marcaRepo->find($marca->id);

        $dbMarca = $dbMarca->toArray();
        $this->assertModelData($marca->toArray(), $dbMarca);
    }

    /**
     * @test update
     */
    public function test_update_marca()
    {
        $marca = factory(Marca::class)->create();
        $fakeMarca = factory(Marca::class)->make()->toArray();

        $updatedMarca = $this->marcaRepo->update($fakeMarca, $marca->id);

        $this->assertModelData($fakeMarca, $updatedMarca->toArray());
        $dbMarca = $this->marcaRepo->find($marca->id);
        $this->assertModelData($fakeMarca, $dbMarca->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_marca()
    {
        $marca = factory(Marca::class)->create();

        $resp = $this->marcaRepo->delete($marca->id);

        $this->assertTrue($resp);
        $this->assertNull(Marca::find($marca->id), 'Marca should not exist in DB');
    }
}
