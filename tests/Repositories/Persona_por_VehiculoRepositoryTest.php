<?php namespace Tests\Repositories;

use App\Models\Persona_por_Vehiculo;
use App\Repositories\Persona_por_VehiculoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class Persona_por_VehiculoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var Persona_por_VehiculoRepository
     */
    protected $personaPorVehiculoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->personaPorVehiculoRepo = \App::make(Persona_por_VehiculoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->make()->toArray();

        $createdPersona_por_Vehiculo = $this->personaPorVehiculoRepo->create($personaPorVehiculo);

        $createdPersona_por_Vehiculo = $createdPersona_por_Vehiculo->toArray();
        $this->assertArrayHasKey('id', $createdPersona_por_Vehiculo);
        $this->assertNotNull($createdPersona_por_Vehiculo['id'], 'Created Persona_por_Vehiculo must have id specified');
        $this->assertNotNull(Persona_por_Vehiculo::find($createdPersona_por_Vehiculo['id']), 'Persona_por_Vehiculo with given id must be in DB');
        $this->assertModelData($personaPorVehiculo, $createdPersona_por_Vehiculo);
    }

    /**
     * @test read
     */
    public function test_read_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();

        $dbPersona_por_Vehiculo = $this->personaPorVehiculoRepo->find($personaPorVehiculo->id);

        $dbPersona_por_Vehiculo = $dbPersona_por_Vehiculo->toArray();
        $this->assertModelData($personaPorVehiculo->toArray(), $dbPersona_por_Vehiculo);
    }

    /**
     * @test update
     */
    public function test_update_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();
        $fakePersona_por_Vehiculo = factory(Persona_por_Vehiculo::class)->make()->toArray();

        $updatedPersona_por_Vehiculo = $this->personaPorVehiculoRepo->update($fakePersona_por_Vehiculo, $personaPorVehiculo->id);

        $this->assertModelData($fakePersona_por_Vehiculo, $updatedPersona_por_Vehiculo->toArray());
        $dbPersona_por_Vehiculo = $this->personaPorVehiculoRepo->find($personaPorVehiculo->id);
        $this->assertModelData($fakePersona_por_Vehiculo, $dbPersona_por_Vehiculo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_persona_por__vehiculo()
    {
        $personaPorVehiculo = factory(Persona_por_Vehiculo::class)->create();

        $resp = $this->personaPorVehiculoRepo->delete($personaPorVehiculo->id);

        $this->assertTrue($resp);
        $this->assertNull(Persona_por_Vehiculo::find($personaPorVehiculo->id), 'Persona_por_Vehiculo should not exist in DB');
    }
}
