<?php

namespace App\Http\Controllers;

use App\DataTables\Persona_por_VehiculoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePersona_por_VehiculoRequest;
use App\Http\Requests\UpdatePersona_por_VehiculoRequest;
use App\Repositories\Persona_por_VehiculoRepository;
use App\Http\Controllers\AppBaseController;
use Flash;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\View\View;
use Response;

class Persona_por_VehiculoController extends AppBaseController
{
    /** @var  Persona_por_VehiculoRepository */
    private $personaPorVehiculoRepository;

    public function __construct(Persona_por_VehiculoRepository $personaPorVehiculoRepo)
    {
        $this->personaPorVehiculoRepository = $personaPorVehiculoRepo;
    }

    /**
     * Display a listing of the Persona_por_Vehiculo.
     *
     * @param Persona_por_VehiculoDataTable $personaPorVehiculoDataTable
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function index(Persona_por_VehiculoDataTable $personaPorVehiculoDataTable)
    {
        return $personaPorVehiculoDataTable->render('persona_por__vehiculos.index');
    }

    /**
     * Show the form for creating a new Persona_por_Vehiculo.
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function create()
    {
        return view('persona_por__vehiculos.create');
    }

    /**
     * Store a newly created Persona_por_Vehiculo in storage.
     *
     * @param CreatePersona_por_VehiculoRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function store(CreatePersona_por_VehiculoRequest $request)
    {
        $input = $request->all();

        $personaPorVehiculo = $this->personaPorVehiculoRepository->create($input);

        Flash::success('Persona Por  Vehiculo saved successfully.');

        return redirect(route('personaPorVehiculos.index'));
    }

    /**
     * Display the specified Persona_por_Vehiculo.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function show($id)
    {
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            Flash::error('Persona Por  Vehiculo not found');

            return redirect(route('personaPorVehiculos.index'));
        }

        return view('persona_por__vehiculos.show')->with('personaPorVehiculo', $personaPorVehiculo);
    }

    /**
     * Show the form for editing the specified Persona_por_Vehiculo.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function edit($id)
    {
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            Flash::error('Persona Por  Vehiculo not found');

            return redirect(route('personaPorVehiculos.index'));
        }

        return view('persona_por__vehiculos.edit')->with('personaPorVehiculo', $personaPorVehiculo);
    }

    /**
     * Update the specified Persona_por_Vehiculo in storage.
     *
     * @param  int              $id
     * @param UpdatePersona_por_VehiculoRequest $request
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function update($id, UpdatePersona_por_VehiculoRequest $request)
    {
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            Flash::error('Persona Por  Vehiculo not found');

            return redirect(route('personaPorVehiculos.index'));
        }

        $personaPorVehiculo = $this->personaPorVehiculoRepository->update($request->all(), $id);

        Flash::success('Persona Por  Vehiculo updated successfully.');

        return redirect(route('personaPorVehiculos.index'));
    }

    /**
     * Remove the specified Persona_por_Vehiculo from storage.
     *
     * @param  int $id
     *
     * @return Response|Factory|RedirectResponse|Redirector|View
     */
    public function destroy($id)
    {
        $personaPorVehiculo = $this->personaPorVehiculoRepository->find($id);

        if (empty($personaPorVehiculo)) {
            Flash::error('Persona Por  Vehiculo not found');

            return redirect(route('personaPorVehiculos.index'));
        }

        $this->personaPorVehiculoRepository->delete($id);

        Flash::success('Persona Por  Vehiculo deleted successfully.');

        return redirect(route('personaPorVehiculos.index'));
    }
}
