<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMarcaAPIRequest;
use App\Http\Requests\API\UpdateMarcaAPIRequest;
use App\Models\Marca;
use App\Repositories\MarcaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MarcaController
 * @package App\Http\Controllers\API
 */

class MarcaAPIController extends AppBaseController
{
    /** @var  MarcaRepository */
    private $marcaRepository;

    public function __construct(MarcaRepository $marcaRepo)
    {
        $this->marcaRepository = $marcaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/marcas",
     *      summary="Get a listing of the Marcas.",
     *      tags={"Marca"},
     *      description="Get all Marcas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Marca")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $marcas = $this->marcaRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($marcas->toArray(), 'Marcas retrieved successfully');
    }

    /**
     * @param CreateMarcaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/marcas",
     *      summary="Store a newly created Marca in storage",
     *      tags={"Marca"},
     *      description="Store Marca",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Marca that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Marca")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Marca"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateMarcaAPIRequest $request)
    {
        $input = $request->all();

        $marca = $this->marcaRepository->create($input);

        return $this->sendResponse($marca->toArray(), 'Marca saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/marcas/{id}",
     *      summary="Display the specified Marca",
     *      tags={"Marca"},
     *      description="Get Marca",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Marca",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Marca"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Marca $marca */
        $marca = $this->marcaRepository->find($id);

        if (empty($marca)) {
            return $this->sendError('Marca not found');
        }

        return $this->sendResponse($marca->toArray(), 'Marca retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateMarcaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/marcas/{id}",
     *      summary="Update the specified Marca in storage",
     *      tags={"Marca"},
     *      description="Update Marca",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Marca",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Marca that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Marca")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Marca"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateMarcaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Marca $marca */
        $marca = $this->marcaRepository->find($id);

        if (empty($marca)) {
            return $this->sendError('Marca not found');
        }

        $marca = $this->marcaRepository->update($input, $id);

        return $this->sendResponse($marca->toArray(), 'Marca updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/marcas/{id}",
     *      summary="Remove the specified Marca from storage",
     *      tags={"Marca"},
     *      description="Delete Marca",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Marca",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Marca $marca */
        $marca = $this->marcaRepository->find($id);

        if (empty($marca)) {
            return $this->sendError('Marca not found');
        }

        $marca->delete();

        return $this->sendSuccess('Marca deleted successfully');
    }
}
