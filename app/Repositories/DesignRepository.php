<?php

namespace App\Repositories;

use App\Models\Design;
use App\Repositories\BaseRepository;

/**
 * Class DesignRepository
 * @package App\Repositories
 * @version February 19, 2020, 3:36 am UTC
*/

class DesignRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'categories_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Design::class;
    }
}
