<?php

namespace App\Repositories;

use App\Models\Persona_por_Vehiculo;
use App\Repositories\BaseRepository;

/**
 * Class Persona_por_VehiculoRepository
 * @package App\Repositories
 * @version March 6, 2021, 4:59 am UTC
*/

class Persona_por_VehiculoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'vehiculos_id',
        'personas_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Persona_por_Vehiculo::class;
    }
}
