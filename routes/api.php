<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function (){
    Route::resource('users', 'UserAPIController');

    Route::resource('categories', 'CategoryAPIController');

    Route::resource('configurations', 'ConfigurationAPIController');

    Route::resource('designs', 'DesignAPIController');
    Route::post('v1/loginuser', 'UserAPIController@loginuser');



    Route::resource('layers', 'LayerAPIController');

    Route::resource('personas', 'PersonaAPIController');

    Route::resource('rols', 'RolAPIController');

    Route::resource('ciudads', 'CiudadAPIController');

    Route::resource('vehiculos', 'VehiculoAPIController');

    Route::resource('persona_por__vehiculos', 'Persona_por_VehiculoAPIController');

    Route::resource('marcas', 'MarcaAPIController');
});