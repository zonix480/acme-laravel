<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonaPorVehiculo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona_por_vehiculo', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('vehiculos_id');
            $table->integer('personas_id');
            $table->timestamps();
            $table->timestamp("deleted_at")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona_por_vehiculo');
    }
}
