<?php

use Illuminate\Database\Seeder;

class PersonaPorVehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<20;$i++){
            DB::table('persona_por_vehiculo')->insert([
                'vehiculos_id'      => $faker->numberBetween(1,10),
                'personas_id'  =>  $faker->numberBetween(1,20),
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
    }
}
