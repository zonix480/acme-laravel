<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
class PersonasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $pass = Hash::make('123456789');
        for($i=0;$i<10;$i++){
        // Create record 
        DB::table('personas')->insert([
            'cedula'      => $faker->numberBetween(100000,999999),
            'primer_nombre'  =>  $faker->firstName,
            'segundo_nombre'  =>  $faker->lastName,
            'apellidos'  =>  $faker->lastName,
            'email'     => $faker->email,
            'direccion' => $faker->address,
            'contraseña'  => $pass,
            'rols_id'  => $faker->numberBetween(1,2),
            'telefono'     => $faker->phoneNumber,
            'ciudad_id'      => $faker->numberBetween(1,20),
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
        }
    }
}
