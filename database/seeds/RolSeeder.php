<?php

use Illuminate\Database\Seeder;
use Faker\Generator;
use Illuminate\Support\Carbon;
class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Generator $faker)
    {
        // Create record 
        DB::table('rols')->insert([
            'nombre'      =>"propietario",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
        DB::table('rols')->insert([
            'nombre'      =>"vendedor",
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
    }
}
