<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MarcaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marcas = array(
            "BMW",
            "Mini",
            "Land Rover",
            "Kia",
            "Ferrari",
            "Nissan",
            "Porsche",
            "Mercedes Benz",
            "Toyota",
        );
        foreach ($marcas as $key) {
            DB::table('marcas')->insert([
                'nombre'      => $key,
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now()
            ]);
        }
    }
}
